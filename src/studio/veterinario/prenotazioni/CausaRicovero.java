package studio.veterinario.prenotazioni;

public enum CausaRicovero
{
	FRATTURA ("Frattura"), 
	FEBBRE ("Febbre"), 
	INDIGESTIONE ("Indigestione");

	private String causa;

	CausaRicovero(String causa)
	{
		this.setCausa(causa);
		
	}

	public String getCausa()
	{
		return causa;
	}

	public void setCausa(String causa)
	{
		this.causa = causa;
	}
	
	
}
