package studio.veterinario.prenotazioni;

public enum TipoAnimale
{
	CANE (true, "Cane"), 
	GATTO (true, "Gatto"), 
	POLLO (false, "Pollo"), 
	CONIGLIO (false, "Coniglio");

	private boolean domestico;
	private String tipo;


	TipoAnimale(boolean domestico, String tipo)
	{
		this.domestico = domestico;
		this.setTipo(tipo);
		// TODO Auto-generated constructor stub
	}

	public boolean isDomestico()
	{
		return domestico;
	}

	public void setDomestico(boolean domestico)
	{
		this.domestico = domestico;
	}

	public String getTipo()
	{
		return tipo;
	}

	public void setTipo(String tipo)
	{
		this.tipo = tipo;
	}
	
	
	
}
