package studio.veterinario.prenotazioni;

public enum Orario
{
	PRIMO_SLOT ("8:00 / 9:00"),
	SECONDO_SLOT ("9:00 / 10:00"),
	TERZO_SLOT ("10:00 / 11:00"),
	QUARTO_SLOT ("11:00 / 12:00");

	
	
	private String orario;

	Orario(String orario)
	{
		this.setOrario(orario);
	}

	public String getOrario()
	{
		return orario;
	}

	public void setOrario(String orario)
	{
		this.orario = orario;
	}
	
	
}
