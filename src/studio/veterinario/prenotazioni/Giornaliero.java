package studio.veterinario.prenotazioni;

public class Giornaliero
{
	
	// attributi
	public Prenotazione[] schedule = new Prenotazione[4];
	
	// metodi
	public Prenotazione[] crea_giornaliero()
	{

		System.out.println("Data odierna\n");
		
		Prenotazione prima = new Prenotazione(Orario.PRIMO_SLOT, true, CausaRicovero.FEBBRE, TipoAnimale.CANE, "17/5/2020", "Fido");
		schedule[0] = prima;
		
		Prenotazione seconda = new Prenotazione(Orario.SECONDO_SLOT, true, CausaRicovero.INDIGESTIONE, TipoAnimale.POLLO, "17/1/2018", "");
		schedule[1] = seconda;
		
		Prenotazione terza = new Prenotazione(Orario.TERZO_SLOT, true, CausaRicovero.FRATTURA, TipoAnimale.CONIGLIO, "11/1/2019", "");
		schedule[2] = terza;
		
		Prenotazione quarta = new Prenotazione(Orario.QUARTO_SLOT, true, CausaRicovero.INDIGESTIONE, TipoAnimale.GATTO, "17/1/2018", "Kitty");
		schedule[3] = quarta;
		
		
		/*
		Prenotazione quinta = new Prenotazione(Orario.QUINTO_SLOT, true, CausaRicovero.INDIGESTIONE, TipoAnimale.POLLO, "17/1/2018", "");
		schedule[4] = quinta;
		*/
		
		return schedule;
	}

	public void stampa_giornaliero(Prenotazione[] schedule)
	{
		for (Prenotazione prenotazione : schedule)
		{
			System.out.println("Slot orario > " + prenotazione.orario.getOrario());
			System.out.println("Causa Ricovero > " + prenotazione.causaRicovero.getCausa());
			System.out.println("Tipo animale > " + prenotazione.tipoAnimale.getTipo());
			System.out.println("Data di nascita > " + prenotazione.dataNascita);
			if (prenotazione.tipoAnimale.isDomestico())
			{
				System.out.println("Nome animale > " + prenotazione.nome);
			}
			System.out.println();
		}
		
	}
	
}
