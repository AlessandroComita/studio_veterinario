package studio.veterinario.prenotazioni;

public class Prenotazione
{
	// ATTRIBUTI
	
	Orario orario;
	Boolean assegnato;
	CausaRicovero causaRicovero;
	TipoAnimale tipoAnimale;
	String dataNascita;
	String nome;

	
	// COSTRUTTORE
	
	
	public Prenotazione(Orario orario, Boolean assegnato, CausaRicovero causaRicovero, TipoAnimale tipoAnimale,
			String dataNascita, String nome)
	{
		super();
		this.orario = orario;
		this.assegnato = assegnato;
		this.causaRicovero = causaRicovero;
		this.tipoAnimale = tipoAnimale;
		this.dataNascita = dataNascita;
		this.nome = nome;
	}

	
	
	
	
	// GETTERS E SETTERS

	public Orario getOrario()
	{
		return orario;
	}


	public void setOrario(Orario orario)
	{
		this.orario = orario;
	}


	public Boolean getAssegnato()
	{
		return assegnato;
	}


	public void setAssegnato(Boolean assegnato)
	{
		this.assegnato = assegnato;
	}


	public CausaRicovero getCausaRicovero()
	{
		return causaRicovero;
	}


	public void setCausaRicovero(CausaRicovero causaRicovero)
	{
		this.causaRicovero = causaRicovero;
	}


	public TipoAnimale getTipoAnimale()
	{
		return tipoAnimale;
	}


	public void setTipoAnimale(TipoAnimale tipoAnimale)
	{
		this.tipoAnimale = tipoAnimale;
	}


	public String getDataNascita()
	{
		return dataNascita;
	}


	public void setDataNascita(String dataNascita)
	{
		this.dataNascita = dataNascita;
	}


	public String getNome()
	{
		return nome;
	}


	public void setNome(String nome)
	{
		this.nome = nome;
	}
	
	
	

	
	
	
}
